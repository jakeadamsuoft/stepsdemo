/**
 * @format
 */

import {AppRegistry} from 'react-native';
import JakesApp from './JakesApp';
import {name as appName} from './app.json';




AppRegistry.registerComponent(appName, () => JakesApp);
