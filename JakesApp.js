/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import { Text, Button } from 'react-native';

import healthKit from 'rn-apple-healthkit';

// include all necessary healthkit user permissions here
let options = {
  permissions: {
      read: ["StepCount", "Steps", "BloodGlucose"],
      write: ["StepCount", "Steps"]
  }
};

//temporarily placed this here: obtains the current date for fetching step data
let d = new Date();
    let today = {
      date: d.toISOString()
    };

// blood glucose parameters
let y = new Date();
y.setDate(d.getDate() -1);
let BG_options = {
  unit: 'mgPerdL',	// optional; default 'mmolPerL'
  startDate: (y).toISOString(), // required
  endDate: (new Date()).toISOString(), // optional; default now
  ascending: false, // optional; default false
  limit: 1,	// optional; default no limit
};

class JakesApp extends Component {

  constructor (props) {
    super(props);
    }
  
  state = {
    stepCount: NaN,
    BloodGlucose: NaN
  }

  componentDidMount() {

// initialize the healthkit with permissions
healthKit.initHealthKit(options, (err, results) => {
  if (err) {
      console.log("error initializing Healthkit: ", err);
      return; }
  });
}

// update the state containing the current step count and blood glucose readings.
updateState = () => 

  {healthKit.getStepCount(today, (err,res) => {

    {console.debug(res.value)};
    this.setState({stepCount: res.value})
})

  healthKit.getBloodGlucoseSamples(BG_options, (err,res) => {

    {console.debug(res)};
    this.setState({BloodGlucose: res[0]})
  })
  }

  render() {
    return (
      <View style={styles.container}>
        {/* View is used here to wrap all component in one */}
        <Text style={{textAlignVertical: "center",textAlign: "center"}}>Today so far you've accumulated {this.state.stepCount} step(s) and your most recent blood glucose reading was {this.state.BloodGlucose.value} mg/dL at {this.state.BloodGlucose.startDate}. Press the button below to resync.</Text> 
        {/* Button whith handler function named onPressLearnMore*/}
        <Button
         onPress={this.updateState}
         title="Fetch"
         color="#841584"
        />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

});


export default JakesApp;